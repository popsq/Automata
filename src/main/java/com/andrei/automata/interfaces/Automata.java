package com.andrei.automata.interfaces;

import java.io.Serializable;

/**
 * Automata
 *
 * @param <INPUT> input type
 */
public interface Automata<STATE extends State, INPUT> extends Serializable {

    /**
     * Add state to set
     *
     * @param state input state
     */
    public void addState(String state);

    /**
     * Check if state exists in set already
     *
     * @param state input state
     * @return true if state exists, false if it doesn't
     */
    public boolean checkStateExists(STATE state);

    /**
     * Adds transition to set
     *
     * @param stState  starting state
     * @param endState ending state
     * @param symbol   transition symbol
     */
    public void addTransition(STATE stState, STATE endState, INPUT symbol);

    /**
     * Check if input is accepted by automata
     *
     * @param input input word
     * @return true if accepted by automata, false if it isn't
     */
    public boolean isAcceptedInput(INPUT input);

}
