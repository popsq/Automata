package com.andrei.automata.interfaces;

import java.io.Serializable;
import java.util.Map;

/**
 * State interface
 *
 * @param <TRANSITION> transition type
 * @param <SYMBOL>     symbol type
 *
 *                     Created by Popescu on 7/9/2014.
 */
public interface State<TRANSITION extends Transition, SYMBOL> extends Serializable {

    /**
     * Returns True if starting state, false if it isn't
     */
    public boolean isStartState();

    /**
     * Returns True if final state, false if it isn't
     */
    public boolean isFinalState();

    /**
     * Returns true if stateName is "Invalid", false if it isn't
     */
    public boolean isInvalid();

    /**
     * Returns state name
     */
    public String getStateName();

    /**
     * Returns transition from state
     *
     * @param symbol input symbol
     * @return returns transition if found, invalid if it isn't
     */
    public Map<String, TRANSITION> findTransitions(SYMBOL symbol);

    /**
     * Checks if transition exists already in set
     *
     * @param transition input transition
     * @return true if it exists already, false if it doesn't
     */
    public boolean checkTransitionExists(Transition transition);

}
