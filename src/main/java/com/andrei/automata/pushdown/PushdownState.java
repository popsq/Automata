package com.andrei.automata.pushdown;

import com.andrei.automata.finite.FiniteState;
import com.andrei.automata.interfaces.Transition;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Popescu on 8/6/2014.
 */
public class PushdownState<TYPE> extends FiniteState {

    private final Map<String, PushdownTransition> transitions = new HashMap<>();

    public PushdownState(TYPE stateName, Boolean start, Boolean fin) {
        super(stateName, start, fin);
    }

    /**
     * Returns if input is the same name as prezent state
     *
     * @param obj input name
     * @return true if both state have the same name, false if they don't
     */
    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj.getClass() == this.getClass()) {
            PushdownState state = (PushdownState) obj;
            return state.getStateName().equals(this.getStateName());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    /**
     * Checks if transition exists already in set
     *
     * @param transition input transition
     * @return true if it exists already, false if it doesn't
     */
    @Override
    public boolean checkTransitionExists(Transition transition) {
        return transitions.containsValue(transition);
    }

    /**
     * Returns transitions for state
     *
     * @return transitions set
     */
    public Map<String, PushdownTransition> getTransitions() {
        return transitions;
    }

    /**
     * Returns transition from state
     *
     * @param inputSymbol input symbol
     * @return transition with symbol
     */
    @Override
    public Map<String, PushdownTransition> findTransitions(Object inputSymbol) {

        Map<String, PushdownTransition> tempTranstions = new HashMap<>();
        if (inputSymbol != null)
            transitions.entrySet()
                    .stream()
                    .filter(transition -> inputSymbol.hashCode() == transition.getValue().getSymbol().hashCode() ||
                            transition.getValue().getSymbol() == "*")
                    .forEach(transition -> tempTranstions.put(String.valueOf(transition.getValue().hashCode()), transition.getValue()));
        if (tempTranstions.isEmpty())
            tempTranstions.put("Invalid", new PushdownTransition<>(new PushdownState<>("Invalid", false, false), "", "", ""));
        return tempTranstions;
    }

    /**
     * String format:
     * <p/>
     * State: <stateName> (start)|(final)|blank
     * <transitions>
     *
     * @return formatted string
     */
    @Override
    public String toString() {
        StringBuilder returnStr = new StringBuilder("State: " + super.getStateName());
        if (isStartState()) returnStr.append(" (start) ");
        if (isFinalState()) returnStr.append(" (final) ");
        for (Map.Entry<String, PushdownTransition> transition : this.transitions.entrySet())
            returnStr.append("\n").append(transition.getValue());
        return returnStr.toString();
    }
}
