package com.andrei.automata.pushdown;

import com.andrei.automata.listeners.PrintToConsole;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Popescu on 8/6/2014.
 */
public class PushdownAutomataTest {

    @Test
    public void PDATest() throws IOException, URISyntaxException {
        Path inputPath = Paths.get(this.getClass().getResource("/pdinput").toURI());
        PushdownAutomata<String> automata = new PushdownAutomata<>(inputPath.toFile());

        if (automata.isAccessibleFile()) {
            PrintToConsole printToConsole = new PrintToConsole();

            automata.addObserver(printToConsole);
            automata.build();

            //PDA -> {0^n1^n | n >= 1}
            Assert.assertFalse(automata.isAcceptedInput("0000111"));
            Assert.assertTrue(automata.isAcceptedInput("000111"));
        }
    }

    @Test
    public void printPDAutomataTest() throws IOException, URISyntaxException {
        Path inputPath = Paths.get(this.getClass().getResource("/pdinput").toURI());
        PushdownAutomata<String> automata = new PushdownAutomata<>(inputPath.toFile());

        if (automata.isAccessibleFile()) {
            PrintToConsole printToConsole = new PrintToConsole();

            automata.addObserver(printToConsole);
            automata.build();
        }

        automata.notifyObservers(automata);
    }
}
